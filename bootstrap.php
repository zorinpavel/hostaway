<?php
    
    require_once __DIR__."/vendor/autoload.php";
    
    use Doctrine\ORM\Tools\Setup;
    use Doctrine\ORM\EntityManager;
    use Config\CamelCaseNamingStrategy;
    
    $isDevMode = true;
    $config = \Doctrine\ORM\Tools\Setup::createAnnotationMetadataConfiguration(array(__DIR__."/entities"), $isDevMode, null, null, false);
    
    $namingStrategy = new CamelCaseNamingStrategy();
    $config->setNamingStrategy($namingStrategy);
    
    $dbParams = array(
        'driver'   => 'pdo_mysql',
        'user'     => 'hostaway',
        'password' => 'T9a1R7o6',
        'dbname'   => 'hostaway',
        'host'     => 'localhost',
    );
    
    $entityManager = \Doctrine\ORM\EntityManager::create($dbParams, $config);
    
    return $entityManager;
    
    