<?php
    
    namespace Entities;
    
    
    use DateTime;
    use \Doctrine\ORM\Mapping as ORM;
    use GuzzleHttp\Client;
    use JsonSerializable;
    use libphonenumber\NumberParseException;
    use libphonenumber\PhoneNumberFormat;
    use libphonenumber\PhoneNumberUtil;

    /**
     * @ORM\Entity
     * @ORM\Table(name="contacts")
     */
    class Contact implements JsonSerializable {
    
        /**
         * @ORM\Id
         * @ORM\Column(type="integer")
         * @ORM\GeneratedValue
         */
        private $id;
        
        /**
         * @ORM\Column(type="string")
         */
        private $firstName;
        
        /**
         * @ORM\Column(type="string", nullable=true)
         */
        private $lastName;
        
        /**
         * @ORM\Column(type="string")
         */
        private $phoneNumber;
        
        /**
         * @ORM\Column(type="string", nullable=true)
         */
        private $countryCode;
        
        /**
         * @ORM\Column(type="string", nullable=true)
         */
        private $timeZoneName;
        
        /**
         * @ORM\Column(type="datetime")
         */
        private $insertedOn;
        
        /**
         * @ORM\Column(type="datetime")
         */
        private $updatedOn;
        
        public $validationError;
        
        
        public function getId() {
            return $this->id;
        }
    
    
        public function getFirstName() {
            return $this->firstName;
        }
        
        
        public function setFirstName($name) {
            $this->firstName = $name;
        }
        
        
        public function getLastName() {
            return $this->lastName;
        }
        
        
        public function setLastName($name) {
            $this->lastName = $name;
        }
    
    
        public function getPhoneNumber() {
            return $this->phoneNumber;
        }
    
    
        public function setPhoneNumber($number) {
            $phoneUtil = PhoneNumberUtil::getInstance();
            // try to parse phone number
            try {
                $phoneNumber = $phoneUtil->parse($number);
                if($phoneUtil->isValidNumber($phoneNumber)) {
                    // if is valid format number
                    $this->phoneNumber = $phoneUtil->format($phoneNumber, PhoneNumberFormat::INTERNATIONAL);
                    
                    return true;
                } else
                    $this->validationError = "Phone number is not valid";
            } catch (NumberParseException $e) {
                $this->validationError = "Can't parse phone number";
            }
            
            return false;
        }
        
        
        public function getCountryCode() {
            return $this->countryCode;
        }
        
        
        public function setCountryCode($code) {
            $code = strtoupper($code);
            
            $client = new Client();
            $response = $client->request('GET', 'https://api.hostaway.com/countries');
    
            if($response->getStatusCode() == 200) {
                $body = json_decode($response->getBody());
                if($body->status == "success") {
                    if(isset($body->result->{$code})) {
                        $this->countryCode = $code;
    
                        return true;
                    }
                }
            }
    
            $this->validationError = "Can't parse countryCode";
            return false;
        }
        

        public function getTimeZoneName() {
            return $this->timeZoneName;
        }
        
        
        public function setTimeZoneName($name) {
            $client = new Client();
            $response = $client->request('GET', 'https://api.hostaway.com/timezones');
    
            if($response->getStatusCode() == 200) {
                $body = json_decode($response->getBody());
                if($body->status == "success") {
                    if(isset($body->result->{$name})) {
                        $this->timeZoneName = $name;
    
                        return true;
                    }
                }
            }
    
            $this->validationError = "Can't parse timeZone";
            return false;
        }
        
        
        public function setUpdatedOn() {
            $this->updatedOn = new DateTime("now");
        }
        

        public function setInsertedOn() {
            $this->insertedOn = new DateTime("now");
        }
        

        /*
         * Validation of input data
         */
        public function validate($data) {
            if(!isset($data['firstName']))
                $this->validationError = "First name can't be empty";
            else
                $this->setFirstName($data['firstName']);

            if(isset($data['lastName']))
                $this->setLastName($data['lastName']);
            
            if(!isset($data['phoneNumber']))
                $this->validationError = "Phone number can't be empty";
    
            if(isset($data['phoneNumber']))
                $this->setPhoneNumber($data['phoneNumber']);
    
            if(isset($data['countryCode']))
                $this->setCountryCode($data['countryCode']);

            if(isset($data['timeZoneName']))
                $this->setTimeZoneName($data['timeZoneName']);
            
            $this->setUpdatedOn();
        }
    
    
        /*
         * transform object to json
         * can filter properties
         */
        public function jsonSerialize() {
            return [
                "id" => $this->getId(),
                "firstName" => $this->getFirstName(),
                "lastName" => $this->getLastName(),
                "phoneNumber" => $this->getPhoneNumber(),
                "countryCode" => $this->getCountryCode(),
                "timeZoneName" => $this->getTimeZoneName()
            ];
        }
    
    
        public function __construct() {
            $this->setInsertedOn();
            $this->setUpdatedOn();
        }
        
    }