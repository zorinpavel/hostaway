<?php
    
    require_once __DIR__."/vendor/autoload.php";
    $entityManager = require_once __DIR__."/bootstrap.php";
    
    
    header("Access-Control-Allow-Origin: *");
    header("Access-Control-Allow-Methods: OPTIONS,GET,POST,PUT,DELETE");
    header("Access-Control-Max-Age: 3600");
    header("Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With");
    header("Content-Type: application/json; charset=UTF-8");
    
    $uri = parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH);
    $uri = explode('/', $uri);
    
    if(!isset($uri[1])) {
        header("HTTP/1.1 404 Not Found");
        die;
    }
    
    $controllerName = "Controllers\\".ucfirst(mb_strtolower($uri[1]))."Controller";
    // if we don't have Controller
    if(!class_exists($controllerName)) {
        header("HTTP/1.1 404 Not Found");
        die;
    }
    
    $itemId = isset($uri[2]) ? (int)$uri[2] : false;
    
    $requestMethod = $_SERVER["REQUEST_METHOD"];
    
    
    // pass the request method and ID to the Controller and process the HTTP request:
    $controller = new $controllerName($requestMethod, $itemId);
    $controller->entityManager = $entityManager;
    $controller->processRequest();