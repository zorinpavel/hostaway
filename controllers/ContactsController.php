<?php
    
    namespace Controllers;
    
    
    use \Entities\Contact;
    use \Doctrine\ORM\Query;
    use \Doctrine\ORM\Tools\Pagination\Paginator;
    
    /**
     * Class ContactsController
     * @package Controllers
     *
     * controller for /contacts endpoint
     */
    class ContactsController extends Controller {
    
        public function createOne() {
            $input = (array)json_decode(file_get_contents('php://input'), true);
    
            $contact = new Contact();
            // validate input
            $contact->validate($input);
            if($contact->validationError) {
                $this->statusCode = "HTTP/1.1 400 Bad Request";
                $this->error = $contact->validationError;
                
                return;
            }
            
            $this->entityManager->persist($contact);
            $this->entityManager->flush();

            $this->statusCode = "HTTP/1.1 201 Created";
            $this->Data = json_encode($contact);
        }
        

        public function findOne($itemId) {
            $contactsRepository = $this->entityManager->getRepository('Entities\Contact');
            // find one item by id
            $contact = $contactsRepository->findOneBy(['id' => $itemId]);

            if(!$contact) {
                $this->notFoundResponse();
                
                return;
            }
            
            $this->statusCode = "HTTP/1.1 200 OK";
            $this->Data = json_encode($contact);
        }
    
    
        public function updateOne($itemId) {
            $contactsRepository = $this->entityManager->getRepository('Entities\Contact');
            // find one item by id
            $contact = $contactsRepository->findOneBy(['id' => $itemId]);
    
            if(!$contact) {
                $this->notFoundResponse();
        
                return;
            }
    
            $input = (array)json_decode(file_get_contents('php://input'), true);
            // validate input
            $contact->validate($input);
            if($contact->validationError) {
                $this->statusCode = "HTTP/1.1 400 Bad Request";
                $this->error = $contact->validationError;
        
                return;
            }
    
            $this->entityManager->persist($contact);
            $this->entityManager->flush();
    
    
            $this->statusCode = "HTTP/1.1 200 OK";
            $this->Data = json_encode($contact);
        }


        public function deleteOne($itemId) {
            $contactsRepository = $this->entityManager->getRepository('Entities\Contact');
            // find one item by id
            $contact = $contactsRepository->findOneBy(['id' => $itemId]);
    
            if(!$contact) {
                $this->notFoundResponse();
        
                return;
            }
            
            $this->entityManager->remove($contact);
            $this->entityManager->flush();
            
            $this->statusCode = "HTTP/1.1 200 OK";
        }
    
    
        public function findAll() {
            //set page size
            $pageSize = (isset($_GET['page_size']) && (int)$_GET['page_size'] > 0) ? (int)$_GET['page_size'] : 1;
            //set page number
            $page = (isset($_GET['page']) && (int)$_GET['page'] > 0) ? (int)$_GET['page'] : 1;
            //set query name
            $name = isset($_GET['name']) ? $_GET['name'] : false;
            
            $contactsRepository = $this->entityManager->getRepository('Entities\Contact');
            $queryBuilder = $contactsRepository->createQueryBuilder('c');
            
            if($name)
                $queryBuilder
                    ->where("c.firstName LIKE :firstName")
                    ->setParameter("firstName", "%".$name."%");
    
            // load doctrine Paginator
            $paginator = new Paginator($queryBuilder->getQuery());
    
            // you can get total items
            $totalItems = count($paginator);
    
            // get total pages
            $pagesCount = ceil($totalItems / $pageSize);
    
            // now get one page's items:
            $paginator
                ->getQuery()
                ->setFirstResult($pageSize * ($page - 1)) // set the offset
                ->setMaxResults($pageSize) // set the limit
                ->getResult(Query::HYDRATE_SIMPLEOBJECT);
    
            $this->statusCode = "HTTP/1.1 200 OK";
            $this->Data = [];
            foreach ($paginator as $contact) {
                array_push($this->Data, json_encode($contact));
            }
        }
        
    }