<?php
    
    namespace Controllers;
    
    /*
     * Base controller for all endpoints provides base methods
     */
    class Controller {
    
        private $requestMethod;
        private $itemId;
        
        public $entityManager;
        
        // status header
        public $statusCode = "HTTP/1.1 400 Bad Request";
        public $error      = false;
        // data array
        public $Data = false;
    
        public function processRequest() {
            switch ($this->requestMethod) {
                case "GET":
                    if($this->itemId)
                        $this->findOne($this->itemId);
                    else
                        $this->findAll();
                    break;
                case "POST":
                    $this->createOne();
                    break;
                case "PUT":
                    $this->updateOne($this->itemId);
                    break;
                case "DELETE":
                    $this->deleteOne($this->itemId);
                    break;
                default:
                    $this->notFoundResponse();
                    break;
            }
    
            header($this->statusCode);
            
            if($this->error) {
                echo json_encode(['error' => $this->error]);
                die;
            }
            
            if($this->Data) {
                if(is_array($this->Data)) {
                    echo "[";

                    foreach($this->Data as $Entity)
                        echo $this->parseJson($Entity);
    
                    echo "]";
                } else
                    echo $this->Data;
            }
        }
        
        /*
         * parse multidimensional array to json
         */
        private function parseJson($object, $jsonString = "") {
            if(is_array($object)) {
                foreach($object as $string) {
                    $jsonString .= $this->parseJson($string, $jsonString);
                }
            } else
                $jsonString .= $object;
            
            return $jsonString;
        }
        
        
        public function findOne($itemId) {
            return $this->notFoundResponse();
        }
    
    
        public function findAll() {
            return $this->notFoundResponse();
        }
    
    
        public function createOne() {
            return $this->notFoundResponse();
        }
    
    
        public function updateOne($itemId) {
            return $this->notFoundResponse();
        }
    
    
        public function deleteOne($itemId) {
            return $this->notFoundResponse();
        }
    
    
        public function notFoundResponse() {
            $this->statusCode = "HTTP/1.1 404 Not Found";
        }
        
        
        public function __construct($requestMethod, $itemId = false) {
            $this->requestMethod = $requestMethod;
            $this->itemId = $itemId;
        }
    
    }