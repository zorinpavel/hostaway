<?php
    
    namespace Config;

    
    use Doctrine\ORM\Mapping\NamingStrategy;
    
    class CamelCaseNamingStrategy implements NamingStrategy {
    
        public function referenceColumnName() {
            return 'id';
        }
        
        
        public function embeddedFieldToColumnName($propertyName, $embeddedColumnName, $className = null, $embeddedClassName = null) {
            return $propertyName . ucfirst($embeddedColumnName);
        }
    
    
        public function classToTableName($className) {
            return substr($className, strrpos($className, '\\') + 1);
        }

        public function propertyToColumnName($propertyName, $className = null) {
            return $propertyName;
        }


        public function joinColumnName($propertyName, $className = null) {
            return $propertyName . ucfirst($this->referenceColumnName());
        }


        public function joinTableName($sourceEntity, $targetEntity, $propertyName = null) {
            return strtolower($this->classToTableName($sourceEntity) . ucfirst($this->classToTableName($targetEntity)));
        }


        public function joinKeyColumnName($entityName, $referencedColumnName = null) {
            if (null === $referencedColumnName) {
                $referencedColumnName = $this->referenceColumnName();
            }
            
            return $this->classToTableName($entityName) . ucfirst($referencedColumnName);
        }
        
    }